# HaProxy PgPool PgBouncer

Jalankan systemd untuk container centos7 dengan perintah
```
cd haproxy-pgpool/centos7-systemd
```
```
sh build.sh
```

Jalankan dengan printah
```
cd haproxy-pgpool/haproxy-db
```
```
docker-compose up --build -d
```

Setting dahulu password untuk masuk ke database master dan slave dengan menggunakan enkripsi md5 pada https://www.md5.cz/ dengan format 
```
md5 ( password+user )
```

lalu masukan hasil dari hash ke file haproxy-pgpool/pool-db/config/userlist.txt dengan format
```
"user" "md5+hasilhash"
For user password ==> "md5" + md5(password + username)
example : "user" "md5953c8cf544f7c75eb3a441b801f9f247"
```


Kemudian setting koneksi ke database master dan slave yang terletap pada haproxy-pgpool/pool-db/db-master,dst , mulai dari :
- Nama Node Database (DB-Master / DB-Slave)
- IP Database
- Port Database
- Path directory dari pool-db/config
- IP Lokal DB

Jika sudah di setting coba jalankan dengan perintah
```
cd haproxy-pgpool/pool-db/db-master
```
```
docker-compose up --build -d
```


## Haproxy Statistic
http://ip-server:7000

- `user_default : admin`
- `pass_default : password`

konfigurasi user password terdapat pada file 
`haproxy-pgpool/haproxy-db/storage/config/haproxy.conf`